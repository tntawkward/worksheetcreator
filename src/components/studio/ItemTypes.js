let TypeNames = {
	1: "Short Text",
	2: "Short Answer",
	3: "Image",
	4: "Checkbox",
	5: "Long Answer",
	6: "Multiple Choice",
};
let ItemTypes = {
	SHORT_TEXT: 1,
	SHORT_ANSWER: 2,
	IMAGE: 3,
	CHECKBOX: 4,
	LONG_ANSWER: 5,
	MULTI_CHOICE: 6,
	CLOSE_DIALOG: 9999,
};
let QuestionTypes = [
	{
		id: ItemTypes.SHORT_TEXT,
		name: "Short Text",
		icon: "description",
		desc: "Just a simple paragraph. Used to give a description above or below a question."
	}, {
		id: ItemTypes.SHORT_ANSWER,
		name: "Short Answer",
		icon: "question_answer",
		desc: "A question and then an answer field below."
	}, {
		id: ItemTypes.LONG_ANSWER,
		name: "Long Answer",
		icon: "wrap_text",
		desc: "A question and a text area for the answer below."
	},
	{
		id: ItemTypes.CHECKBOX,
		name: "Checkbox",
		icon: "check_box",
		desc: "A simple box that users can check, by themselves or by you making them to."
	},
	{
		id: ItemTypes.MULTI_CHOICE,
		name: "Multiple Choice",
		icon: "radio",
		desc: "A multiple choice question with an unlimited amount of answers. Displayed as radio buttons."
	},
	{
		id: ItemTypes.CLOSE_DIALOG,
		name: "Cancel",
		icon: "close",
		desc: "",
		close: true
	}
];
export default {
	ItemTypes: ItemTypes,
	TypeNames: TypeNames,
	qTypes: QuestionTypes,
	fields: (id)=>{
		switch(id) {
			case ItemTypes.SHORT_TEXT:
				return {
					/* Just needs label and other default props */
				};
			case ItemTypes.SHORT_ANSWER:
				return {
					answerPlaceholder: true,
					answerFieldStyle: true,
					required: true,
					validation: true,
					clearIcon: true
				};
			case ItemTypes.LONG_ANSWER:
				return {
					answerFieldStyle: true,
					required: true
				};
			case ItemTypes.CHECKBOX:
				return {
					required: true
				};
			case ItemTypes.MULTI_CHOICE:
				return {
					optionsList: true,
					required: true
				};
		}
		return {};
	},
	getDataPlaceholder: (id)=>{
		switch(id) {
			case ItemTypes.SHORT_TEXT:
				return {text: "", placeholderText: "Type here..."};
			case ItemTypes.SHORT_ANSWER:
				return {
					text: "",
					placeholderText: "Type here...",
					answerPlaceholderText: "Your answer..."
				};
			case ItemTypes.LONG_ANSWER:
				return {
					text: "",
					placeholderText: "Type here...",
					answerPlaceholderText: "Your answer..."
				};
			case ItemTypes.CHECKBOX:
				return {
					text: "",
					placeholderText: "Type here...",
					answerDefault: false,
				};
			case ItemTypes.MULTI_CHOICE:
				return {
					text: "",
					placeholderText: "Type here...",
					choices: [
						{text: ""},
						{text: ""},
						{text: ""},
						{text: ""}
					]
				}
		}
	}
}