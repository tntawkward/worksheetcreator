import Vue from 'vue';
import App from './App.vue';
import Vuetify from 'vuetify';
import VueRouter from 'vue-router';

import Index from "@/components/Home";
import Error404 from "@/components/Error404";
import WStudio from "@/components/WStudio";
import SignUp from "@/components/SignIn";
//import SignIn from "@/components/SignIn";
import DeBug from "@/components/debug";
import NewWorkSheet from "@/components/studio/NewWorkSheet.vue";
import AllWorkSheets from "@/components/studio/AllWorkSheets.vue";
import LogOut from "@/components/LogOut.vue";

import colors from 'vuetify/es5/util/colors';

import 'material-design-icons-iconfont/dist/material-design-icons.css';
import 'vuetify/dist/vuetify.min.css';

import * as firebase from "firebase/app";
import "firebase/auth";
import "firebase/firestore";

const VueCookies = require("vue-cookies");

let config = {
    apiKey: "AIzaSyB7r8VtsYyeigGO1_WBg5U6XQdwW6EziOo",
    authDomain: "worksheetcreator.firebaseapp.com",
    databaseURL: "https://worksheetcreator.firebaseio.com",
    projectId: "worksheetcreator",
    storageBucket: "worksheetcreator.appspot.com",
    messagingSenderId: "275616114577"
};
firebase.initializeApp(config);


Vue.config.productionTip = false;
Vue.use(Vuetify, {
    theme: {
        primary: colors.deepPurple.base,
        secondary: colors.pink.base,
        accent: colors.purple.base,
        error: colors.red.base,
        warning: colors.amber.base,
        info: colors.lightBlue.base,
        success: colors.green.base
    }
});
Vue.use(VueRouter);
Vue.use(VueCookies);

const routes = [
    {path: '/', component: Index},
    {path: '/index', component: Index},
    {path: '/404', component: Error404},
    {path: '/studio/new', component: NewWorkSheet},
    {path: '/studio/all', component: AllWorkSheets},
    {path: '/ws/:id/:mode', component: WStudio},
    {path: '/ws/:id/view', component: WStudio},
    {path: '/studio/:id/:mode', component: WStudio},
    //{path: '/sign-up', component: SignUp},
    {path: '/sign-in', component: SignUp},
    {path: '/logout', component: LogOut},
    {path: '/dev', component: DeBug},
    {path: '/*', component: Error404},
];

const router = new VueRouter({
    routes
});

const db = firebase.firestore();
window.db = db;
window.firebase = firebase;

db.settings({
    timestampsInSnapshots: true,
});

firebase.auth().onAuthStateChanged((user) => {
    createVueInstance(user);
});

function createVueInstance(user) {
    window.vm = new Vue({
        render: h => h(App),
        router: router,
        data() {
            return {
                firebase: firebase,
                user: user,
            }
        },
    }).$mount('#app');
    if(window.createFN) {
        window.createFN();
    }
}