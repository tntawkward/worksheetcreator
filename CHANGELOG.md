Changelog
=========
# Version 0.5.5
## Feb 8, 2019
### 77862f94016f88f64d53d69a6734a931689fde66
- Moved some code to `ItemTypes.js` from `WStudio.vue` because it was hard to find earlier
- Removed unnecessary TODO messages
- Added a new ESLint disable comments
- Removed a lot of unused logging
- Added multiple choice control
- Renamed export name of `Home.vue`
- Added a root element to `LogOut.vue`
- Reformatted HTML attributes (wrap always)
- Removed unused watchers
- Made save button only enabled when changes have been made
- Removed `firebaseConfig.js`
- Moved `@vue/cli` to `devDependencies` from `dependencies` in `package.json`
- 
# Version 0.5
## Feb 6, 2019
### 668abebe94d3ee978b5c619afba1ae412978d278
- Added `CHANGELOG`
### 97a4bc946245b5499c68e333976d3baec227437a
- Renamed `CHANGELOG` to `CHANGELOG.md`
### 38682a36e32dc9632f175de140ef213323cce87c
- Updated `vue-cli-service` to 3.4.0
- Added `@vue/cli` to `dependencies`
- Changed all branding from "WorksheetCreator" to "Sapphire"
- Added a logo for Sapphire (and changed the default favicon)
- "Minified" the styling for the <noscript> tag in `index.html`
- Added a parallax to the home page (along with a loader for no reason)
- Made sign-in reload to `/index` instead of `/studio/all`, which redirected back to `/sign-in` for some reason
- Added route: `/index` (Goes to `Home` component, same as `/`)
- Added a key in the `v-for` in `AllWorkSheets.vue`
- Removed `TODO` comment to add checkbox input
- Added a whole new way of staying DRY: `QuestionRender.vue`. It is a component thatis referenced multiple times, so instead of repeating myself, I can just use `<QuestionRender>`
- Removed `vm.methods` Object in `main.js` because it was empty
- Added `hasOwnProperty` checks in 2 loops
- Added viewing responses! You have the option of individual or a summary of all the answers.
- Moved all question rendering code into `QuestionRender.vue`
- Converted the method of updating `this.inputs` in `WStudio.vue` to an event-driven model. (`QuestionRender` triggers an event with params that is handled in `WStudio`)
- Renamed a few functions to be more accurately-named.
- Added scoped styling for `<blockquote>`s in `WStudio` (used for viewing responser's name, email, and submission time in individual viewing)
### 62aaad284cf47c2c59389a70588ab685bea07f0f
- (Part of a merge with 38682a36e32dc9632f175de140ef213323cce87c) 
- Merged `CHANGELOG.md` with the `master` branch


# Version 0.4
## Jan 18, 2019
### 8faafec9ab90125b6067b690a5dcc9646cecef0b
- Added a red `*` after required questions
- Improved checkbox question type
- Added a subtitle underneath `Question {x}` to show what type of question it is (Short Answer, etc.) in the properties `v-menu`
### 83d4850c64644bcc13192a8e2d2e0d32010d8f74
- Added function to determine which properties fields are used by which question to keep this clean and not DRY
- Added checkbox question type
### b8c5472e1bca208f52f455b27d71c912485fad10
- Added circular indeterminate loader for all work sheets view
- Did some more work on the text area component
- Converted some expressions into data in the studio view
- Made the sidebar more dynamic, so it now kind of sticks to the top of the screen...It finds the best fit for your screen and can be scrolled now.
- Saved cols in firebase in save() function

# Version 0.3
## Jan 17, 2019
### 62809bbdca9545852a304b4bf17d47b12abf6945
- Added sign-in protection on /studio/all and /studio/new
- Added new types in ItemTypes and a base for the long answer question type
- Added a route for signing out
- Merged sign up/sign in pages into one (see comment about FirebaseUI)
- Added object for response data (keys are question uuid's and values are the inputted data)
- Made `theme` strictly a boolean (not a string!)
- Removed importing of vue-recaptcha because we don't need it anymore
- Added vue.config.js just in case we want to configure Webpack
- Switched login system front-end over to FirebaseUI (firebase-ui)! Much more stable and already pre-made.


# Version 0.2
## Jan 13, 2019

### 41a6544cfb6e39545318c7d8321de6cb035dfef0
- Added dark/light theme toggle which stores in cookies (Added vue-cookies to handle that)
- Added functionality on `/studio/all` page (My worksheets)
- Added functionality on `/studio/new` page (New worksheet)
- Added the `:mode` dynamic prop to routes (`/studio/:id/:mode`)
### ed3189bfd4259de9a8725d7238132b991c79c07b
- Made questions draggable
- Added saving functionality
- Added `ItemTypes.js`, a way to manage question types more concisely
### 80f737d94941d811f692bf87ca45ddfbd75753b2
- Made the menu a "sidebar" instead of the default `v-menu`
### f4f3db91e30ba3525b22d2b51784ac9389b05f6d
- Added a message at the top of the console to show when `App.vue` was fully reloaded.
- Added helper fields instead of checking if the mode was viewing or editing every time (like `this.editing` and `this.viewing`)
- Disabled text fields when editing their properties
- Checking if user is owner of worksheet before allowing them to edit


# Version 0.1
## Jan 12, 2019
### f2fb7fe45787f71b4acd9c22f539e81fb3ed2c61
- Styled the <noscript> element
- Added more dependencies (VueDraggable, Vuetify, VueReCaptcha, sass-loader, material-design-icons-iconfont, VueRouter)
- Added more devDependancies (cssLoader)
- Added Firebase config to `index.html`
- Added a basic app structure in `App.vue`
- Deleted the base `HelloWorld.vue` component
- Added some basic routes (Home, Sign In, Sign Up, Debug, and the Studio)
- Turned off `Vue.config.productionTip` so we don't get that message clogging up the console
- Customized the Vuetify theme
- Added waiting for Firebase auth before loading Vue

# Version 0.0.1
## Jan 7, 2019
### 4db4e2d287777aeff489363ee979a7e89baa6b5d
- Initial Commit (Basic Project Setup)